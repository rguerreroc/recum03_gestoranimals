package org.iesjoandaustria.animals.control;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ParserTest extends TestCase {
    public ParserTest( String testName ) {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( ParserTest.class );
    }

    public void testParserHelp() {
        String entrada = "help";
        Comanda c = Parser.parse(entrada);
        assertEquals(entrada, c.getNomComanda());
    }
    public void testParserQuit() {
        String entrada = "quit";
        Comanda c = Parser.parse(entrada);
        assertEquals(entrada, c.getNomComanda());
    }
    public void testParserError() {
        String entrada = "add comanda erronia i ja està";
        Comanda c = Parser.parse(entrada);
        assertEquals("error", c.getNomComanda());
    }
    public void testParserList() {
        String entrada = "list";
        Comanda c = Parser.parse(entrada);
        assertEquals(entrada, c.getNomComanda());
        assertEquals(0, c.getNrDArguments());
    }
    public void testParserListCat() {
        String entrada = "list felins";
        Comanda c = Parser.parse(entrada);
        assertEquals("list", c.getNomComanda());
        assertEquals(1, c.getNrDArguments());
        assertEquals("felins", c.getValorDArgument("categoria"));
    }
    public void testParserAdd() {
        String entrada = "add gat felins";
        Comanda c = Parser.parse(entrada);
        assertEquals("add", c.getNomComanda());
        assertEquals(2, c.getNrDArguments());
        assertEquals("felins", c.getValorDArgument("categoria"));
        assertEquals("gat", c.getValorDArgument("nom"));
    }
    public void testParserDel() {
        String entrada = "del gat";
        Comanda c = Parser.parse(entrada);
        assertEquals("del", c.getNomComanda());
        assertEquals(1, c.getNrDArguments());
        assertEquals("gat", c.getValorDArgument("nom"));
    }
    public void testParserDelAmbCategoria() {
        String entrada = "del gat felins";
        Comanda c = Parser.parse(entrada);
        assertEquals("del", c.getNomComanda());
        assertEquals(2, c.getNrDArguments());
        assertEquals("gat", c.getValorDArgument("nom"));
        assertEquals("felins", c.getValorDArgument("categoria"));
    }
    public void testParserAssign() {
        String entrada = "assign gat to felins";
        Comanda c = Parser.parse(entrada);
        assertEquals("assign", c.getNomComanda());
        assertEquals(2, c.getNrDArguments());
        assertEquals("gat", c.getValorDArgument("nom"));
        assertEquals("felins", c.getValorDArgument("categoria_desti"));
    }
    public void testParserAssignAmbCategoria() {
        String entrada = "assign gat mamifers to felins";
        Comanda c = Parser.parse(entrada);
        assertEquals("assign", c.getNomComanda());
        assertEquals(3, c.getNrDArguments());
        assertEquals("gat", c.getValorDArgument("nom"));
        assertEquals("mamifers", c.getValorDArgument("categoria_origen"));
        assertEquals("felins", c.getValorDArgument("categoria_desti"));
    }
    public void testParserLocSearchSala() {
        String entrada = "loc search salablava";
        Comanda c = Parser.parse(entrada);
        assertEquals("loc-search", c.getNomComanda());
        assertEquals(4, c.getNrDArguments());
        assertEquals("salablava", c.getValorDArgument("sala"));
        assertEquals(null, c.getValorDArgument("passadís"));
        assertEquals(null, c.getValorDArgument("armari"));
        assertEquals(null, c.getValorDArgument("prestatge"));
    }
    public void testParserLocSearchPassadis() {
        String entrada = "loc search salablava 13";
        Comanda c = Parser.parse(entrada);
        assertEquals("loc-search", c.getNomComanda());
        assertEquals(4, c.getNrDArguments());
        assertEquals("salablava", c.getValorDArgument("sala"));
        assertEquals("13", c.getValorDArgument("passadís"));
        assertEquals(null, c.getValorDArgument("armari"));
        assertEquals(null, c.getValorDArgument("prestatge"));
    }
    public void testParserLocSearchArmari() {
        String entrada = "loc search salablava 13 21";
        Comanda c = Parser.parse(entrada);
        assertEquals("loc-search", c.getNomComanda());
        assertEquals(4, c.getNrDArguments());
        assertEquals("salablava", c.getValorDArgument("sala"));
        assertEquals("13", c.getValorDArgument("passadís"));
        assertEquals("21", c.getValorDArgument("armari"));
        assertEquals(null, c.getValorDArgument("prestatge"));
    }
    public void testParserLocSearchPrestatge() {
        String entrada = "   loc    search salablava   13    21    33   ";
        Comanda c = Parser.parse(entrada);
        assertEquals("loc-search", c.getNomComanda());
        assertEquals(4, c.getNrDArguments());
        assertEquals("salablava", c.getValorDArgument("sala"));
        assertEquals("13", c.getValorDArgument("passadís"));
        assertEquals("21", c.getValorDArgument("armari"));
        assertEquals("33", c.getValorDArgument("prestatge"));
    }
}

