package org.iesjoandaustria.animals.control;
/*
 * Encapsula un parser de les comandes del gestor d'animals
 */
import java.util.regex.Pattern;
import java.util.regex.Matcher;

class Parser {
    private static final Pattern[] cmds = {
        /* case 00*/Pattern.compile("^\\s*help\\s*$"),
        /* case 01*/Pattern.compile("^\\s*quit\\s*$"),
        /* case 02*/Pattern.compile("^\\s*list\\s*(\\s(\\w+))?\\s*$"),
        /* case 03*/Pattern.compile("^\\s*add\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s*$"),
        /* case 04*/Pattern.compile("^\\s*del\\s+(\\w+)\\s*(\\s(\\w+))?\\s*$"),
        /* case 05*/Pattern.compile("^\\s*assign\\s+(\\w+)\\s*(\\s(\\w+))?\\s+to\\s+(\\w+)$"),
        /* case 06*/Pattern.compile("^\\s*hab\\s+(\\w+)\\s*(\\s(\\w+))?\\s*add\\s+(\\w+)\\s*$"),
        /* case 07*/Pattern.compile("^\\s*hab\\s+(\\w+)\\s*(\\s(\\w+))?\\s*del\\s+(\\w+)\\s*$"),
        /* case 08*/Pattern.compile("^\\s*hab\\s+(\\w+)\\s*(\\s(\\w+))?\\s*show\\s*$"),
        /* case 09*/Pattern.compile("^\\s*loc\\s+(\\w+)\\s*(\\s(\\w+))?\\s*set\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s*$"),
        /* case 10*/Pattern.compile("^\\s*loc\\s+(\\w+)\\s*(\\s(\\w+))?\\s*unset\\s*$"),
        /* case 11*/Pattern.compile("^\\s*loc\\s+(\\w+)\\s*(\\s(\\w+))?\\s*show\\s*$"),
        /* case 12*/Pattern.compile("^\\s*loc\\s+search\\s+(\\w+)(\\s+(\\d+)(\\s+(\\d+)(\\s+(\\d+))?)?)?\\s*$"),
        /* case 13*/Pattern.compile("^\\s*repo\\s+(\\w+)\\s*(\\s(\\w+))?\\s*$")
    };
    /** composa una comanda a partir de la cadena d'entrada */
    public static Comanda parse(String entrada) {
        int cmdpos;
        Matcher m = null;
        for (cmdpos=0; cmdpos < cmds.length; cmdpos++) {
            Pattern p = cmds[cmdpos];
            m = p.matcher(entrada);
            if (m.find()) {
                break;
            }
        }
        Comanda resultat = null;
        switch (cmdpos) {
            case 0: resultat = new CmdHelp(); break;
            case 1: resultat = new CmdQuit(); break;
            case 2: if (m.group(1) == null) {
                        resultat = new CmdList();
                    } else {
                        String categoria = m.group(2);
                        resultat = new CmdList(categoria);
                    }
                    break;
            case 3: resultat = new CmdAdd(m.group(1), m.group(2), m.group(3));
                    break;
            case 4: if (m.group(2) == null) {
                        resultat = new CmdDel(m.group(1));
                    } else {
                        resultat = new CmdDel(m.group(1), m.group(3));
                    }
                    break;
            case 5: if (m.group(2) == null) {
                        resultat = new CmdAssign(m.group(1), m.group(4));
                    } else {
                        resultat = new CmdAssign(m.group(1), m.group(3), m.group(4));
                    };
                    break;
            case 6: if (m.group(2) == null) {
                        resultat = new CmdHabAdd(m.group(1), m.group(4));
                    } else {
                        resultat = new CmdHabAdd(m.group(1), m.group(3), m.group(4));
                    };
                    break;
            case 8: if(m.group(2) == null){
                        resultat = new CmdHabShow(m.group(1));
                    }else{
                        resultat = new CmdHabShow(m.group(1), m.group(3));
                    }
                    break;
            case 9: if(m.group(2) == null){
                        resultat = new CmdLocSet(m.group(1), m.group(4), m.group(5), m.group(6), m.group(7));
                    }else{
                        resultat = new CmdLocSet(m.group(1), m.group(3), m.group(4), m.group(5), m.group(6), m.group(7));
                    }
                    break;
            case 10: if(m.group(2) == null){
                        resultat = new CmdLocUnset(m.group(1));
                    }else{
                        resultat = new CmdLocUnset(m.group(1), m.group(3));
                    }
                    break;
            case 11: if(m.group(2) == null){
                        resultat = new CmdLocShow(m.group(1));
                    }else{
                        resultat = new CmdLocShow(m.group(1), m.group(3));
                    }
                    break;

                    // case ?: String sala = m.group(1);
                    //         String passadis = m.group(3);
                    //         String armari = m.group(5);
                    //         String prestatge = m.group(7);
                    //         resultat = new CmdLocSearch(sala, passadis, armari, prestatge);
                    //         break;
            case 13: if(m.group(2) == null){
                         resultat = new CmdRepo(m.group(1));
                     }else{
                         resultat = new CmdRepo(m.group(1),m.group(3));

                     }
                     break;

            default: resultat = new CmdError(entrada);
        }
        return resultat;
    }
}
