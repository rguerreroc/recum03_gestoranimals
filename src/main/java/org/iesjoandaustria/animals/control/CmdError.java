package org.iesjoandaustria.animals.control;
/*
 * Comanda Erronia (quan s'ha introduït una comanda desconeguda
 */
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdError extends Comanda {
    public CmdError(String entrada) {
        addArgument("entrada", entrada);
    }
    public String getNomComanda() { return "error"; }
    public boolean executa(Model model, Vista vista) {
        String comanda = getValorDArgument("entrada");

        System.out.println("Ha fallat la comanda: " + comanda);
        return false;
    }
}


