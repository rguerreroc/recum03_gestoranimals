package org.iesjoandaustria.animals.control;
/*
 * Comanda Quit
 */
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdQuit extends Comanda {
    public String getNomComanda() { return "quit"; }
    public boolean executa(Model model, Vista vista) {

        String resultat = vista.composaComiat();
        
        System.out.println(resultat);
        return true;
    }
}

