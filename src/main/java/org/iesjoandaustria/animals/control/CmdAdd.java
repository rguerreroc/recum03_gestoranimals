package org.iesjoandaustria.animals.control;
/*
 * Comanda Add
 */
import org.iesjoandaustria.animals.model.*;
import java.sql.*;
import org.iesjoandaustria.animals.vista.Vista;
class CmdAdd extends Comanda {
    CmdAdd(String nom, String categoria, String reproduccio) {
        addArgument("nom", nom);
        addArgument("categoria", categoria);
        addArgument("reproduccio", reproduccio);
    }
    public String getNomComanda() { return "add"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String cat = getValorDArgument("categoria");
        String repro = getValorDArgument("reproduccio");
        Animal a = new Animal(nom, cat, repro);
        int nouAnimal = 0;
        String resultat = "";
        try{
            nouAnimal = model.addNouAnimal(a);
        }catch(SQLException s){
            System.out.println("Error :" + s);
        }
        
        resultat = vista.composaResposta(nouAnimal);
        System.out.println(resultat);
        return false;

    }
}




