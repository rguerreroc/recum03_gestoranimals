package org.iesjoandaustria.animals.control;

import java.sql.*;
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdLocUnset extends Comanda {
    public CmdLocUnset(String nom){
        addArgument("nom", nom);
    }

    public CmdLocUnset(String nom, String categoria){
        this(nom);
        addArgument("categoria", categoria);
    }

    public String getNomComanda() { return "CmdLocUnset"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String categoria = getValorDArgument("categoria");
        String resultat = "";
        try{
            int codOperacio = model.unsetLoc(nom, categoria);
            resultat = vista.composaResposta(codOperacio);

            System.out.println(resultat);
        }catch(SQLException s){
            System.out.println("Error: "+s);
        }

        return false;
    }

}
