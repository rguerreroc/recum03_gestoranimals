package org.iesjoandaustria.animals.control;
/*
 * Comanda Del
 */
import java.sql.*;
import java.util.*;
import org.iesjoandaustria.animals.model.*;
import org.iesjoandaustria.animals.vista.Vista;
class CmdRepo extends Comanda {
    public CmdRepo(String nom) {
        addArgument("nom", nom);
    }
    public CmdRepo(String nom, String categoria) {
        addArgument("nom", nom);
        addArgument("categoria", categoria);
    }
    public String getNomComanda() { return "repo"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String categoria = getValorDArgument("categoria");
        List<String> animalVista = new ArrayList<String>();
        String resultat = "";
        String localitzacio = ""; 
        Animal a;
        try{
            a = model.repoAnimal(nom, categoria);
            int codOperacion = a.getId();
            if(codOperacion < 0){
                resultat = vista.composaResposta(codOperacion);
            }else{
                localitzacio = model.showLoc(nom, categoria);

                String nomBD = a.getNom();
                animalVista.add(a.getNom());
                animalVista.add(a.getCategoria());
                animalVista.add(a.getReproduccio());
                resultat = vista.composaAnCatRepo(animalVista);
                resultat += "\n";
                if(localitzacio == null){
                    resultat += "Localització:\n\t" + vista.composaResposta(-6);
                }else{
                    if(localitzacio.equals("-3")){
                        resultat += "Localització:\n\t" + vista.composaResposta(-3);
                    }else{
                        resultat += vista.composaLocalitzacio(localitzacio);
                    }
                }
                resultat += "\n";
                List<String> habitat = model.showHab(nom, categoria);
                if(habitat == null){
                    resultat += "Habitat:" +vista.composaResposta(-8);
                }else{
                    if(habitat.equals("-3")){
                        resultat += "Habitat:" +vista.composaResposta(-3);
                    }else{
                        resultat += vista.composaHabitat(habitat);
                    }
                }
            }
            System.out.println(resultat);



        }catch(SQLException s){
            System.out.println("Error: "+s);
        }

        return false;
    }
}
