package org.iesjoandaustria.animals.control;
/*
 * Classe abstracta que encapsula les comandes possibles
 * Les comandes poden tenir arguments i codi per gestionar-les
 */
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
import java.util.*;
import java.sql.*;
abstract class Comanda {
    /** caldrà que cada comanda permeti consultar el seu nom */
    public abstract String getNomComanda();

    /** caldrà que cada comanda sàpiga com interaccionar amb el model
     * i la vista per respondre.
     * La comanda retornarà true si la seva execució implica la
     * finalització de l'aplicació. */
    public abstract boolean executa(Model model, Vista vista) ;

    public Map<String, String> arguments = null;
    public void addArgument(String nom, String valor) {
        if (arguments == null) {
            arguments = new HashMap<String, String>();
        }
        arguments.put(nom, valor);
    }
    /** retorna el nombre d'arguments de la comanda */
    public int getNrDArguments() {
        int resposta = 0;
        if (arguments != null) {
            resposta = arguments.keySet().size();
        }
        return resposta;
    }
    public boolean hasArgument(String nom) {
        boolean resposta = false;
        if (arguments != null) {
            resposta = arguments.containsKey(nom);
        }
        return resposta;
    }
    /** retorna null si no es troba l'argument */
    public String getValorDArgument(String nom) {
        String resposta = null;
        if (arguments != null && arguments.containsKey(nom)) {
            resposta = arguments.get(nom);
        }
        return resposta;
    }
}
