package org.iesjoandaustria.animals.control;

import java.util.Scanner;

import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
/* Implementa la capa de control de l'aplicació */
public class Control {
    private Vista vista = new Vista();
    private Model model = new Model();
    private Scanner teclat = new Scanner(System.in);
    public void run(){
        String entradaCom;
        boolean finalitza = false;
        int manual = 0; // 0 Entrada manual 1 Entrada automatica
        while(!finalitza){
            if(manual == 0){
                System.out.print("[AnimalEnv] ");
                entradaCom = teclat.nextLine();
            }else{
                entradaCom = "repo kiwi";
            }
            Comanda cmd = Parser.parse(entradaCom);
            finalitza = cmd.executa(model, vista);  // en aquesta simulació finalitzem amb aquesta comanda
        }
    }
}
