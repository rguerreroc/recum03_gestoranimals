package org.iesjoandaustria.animals.control;

import java.sql.*;
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdLocSet extends Comanda {
    public CmdLocSet(String nom, String sala, String passadis, String armari, String prestatge){
            addArgument("nom", nom);
            addArgument("sala", sala);
            addArgument("passadis", passadis);
            addArgument("armari", armari);
            addArgument("prestatge", prestatge);
    }
    public CmdLocSet(String nom, String categoria, String sala, String passadis, String armari, String prestatge){
            this(nom, sala, passadis, armari, prestatge);
            addArgument("categoria", categoria);
    }
    public String getNomComanda() { return "CmdLocSet"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String categoria = getValorDArgument("categoria");
        String sala = getValorDArgument("sala");
        String passadis = getValorDArgument("passadis");
        String armari = getValorDArgument("armari");
        String prestatge = getValorDArgument("prestatge");
        String resultat = "";
        try{
            int codOperacio = model.setLoc(nom, categoria, sala, passadis, armari, prestatge);
            resultat = vista.composaResposta(codOperacio);

            System.out.println(resultat);
        }catch(SQLException s){
            System.out.println("Error: "+s);
        }
        return false;
    }
}

