package org.iesjoandaustria.animals.control;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdHabShow extends Comanda {
    public CmdHabShow(String nom){
        addArgument("nom", nom);
    }

    public CmdHabShow(String nom, String categoria){
        this(nom);
        addArgument("categoria", categoria);
    }

    public String getNomComanda() { return "CmdLocUnset"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String categoria = getValorDArgument("categoria");
        String resultat = "";
        try{
            List<String> habitat = model.showHab(nom, categoria);
            if(habitat == null){
                resultat = vista.composaResposta(-8);
            }else{
                if(habitat.equals("-3")){
                    resultat = vista.composaResposta(-3);
                }else{
                    resultat = vista.composaHabitat(habitat);
                }
            }

            System.out.println(resultat);
        }catch(SQLException s){
            System.out.println("Error: " + s);
        }
        return false;
    }

}

