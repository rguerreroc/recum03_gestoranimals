package org.iesjoandaustria.animals.control;
import java.sql.*;
/*
 * Comanda List
 */
import org.iesjoandaustria.animals.model.*;
import org.iesjoandaustria.animals.vista.Vista;
import java.util.*;
class CmdList extends Comanda {
    CmdList() { }   // si no s'indica la categoria
    CmdList(String categoria) {
        addArgument("categoria", categoria);
    }
    public String getNomComanda() { return "list"; }
    public boolean executa(Model model, Vista vista) {
        try{
            List<Animal> animals = model.getLlistaTotsElsAnimals();
            Map<String, List<String[]>> animalsPerVista = new HashMap<String, List<String[]>>();
            // aquí caldria agafar cada animal, però com que és una prova
            // i sabem que només hi ha un, el posem directament
            for (Animal a : animals) {
                String category = a.getCategoria();
                String repro = a.getReproduccio();
                String nom = a.getNom();


                String[] parell = new String[2];
                parell[0] = nom;
                parell[1] = repro;

                if (!animalsPerVista.containsKey(category)) {
                    /* Si la categoria no existia, la crea buida */
                    List<String[]> noms = new ArrayList<String[]>();
                    animalsPerVista.put(category, noms);
                }
                List<String[]> noms = animalsPerVista.get(category);
                noms.add(parell);
            }
            // List<String> nomsAnimals = new ArrayList<String>();
            // nomsAnimals.add("gat");
            // nomsAnimals.add("gos");
            // animalsPerVista.put("felins",nomsAnimals);
            String resultat = vista.composaLlistatAnimals(animalsPerVista);
            System.out.println(resultat);
        }catch(SQLException s){
            System.out.println("Error: "+s);
        }
        return false;   // no finalitza aquí
    }
}



