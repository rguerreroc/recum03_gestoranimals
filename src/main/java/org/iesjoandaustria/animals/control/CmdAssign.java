package org.iesjoandaustria.animals.control;
/*
 * Comanda Del
 */
import java.sql.*;
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdAssign extends Comanda {
    public CmdAssign(String nom, String categoria_desti) {
        addArgument("nom", nom);
        addArgument("catDest", categoria_desti);
    }
    public CmdAssign(String nom, String categoria_origen, String categoria_desti) {
        this(nom, categoria_desti);
        addArgument("catOri", categoria_origen);
    }
    public String getNomComanda() { return "assign"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String catDest = getValorDArgument("catDest");
        String catOri = getValorDArgument("catOri");
        String resultat = "";

        try{
            int codOperacio = model.assingCat(nom,catDest,catOri);
            resultat = vista.composaResposta(codOperacio);

            System.out.println(resultat);
        }catch(SQLException s){
            System.out.println("Error: "+s);
        }
        return false;
    }
}
