package org.iesjoandaustria.animals.control;
/*
 * Comanda Add Habitat
 */
import org.iesjoandaustria.animals.model.*;
import java.sql.*;
import org.iesjoandaustria.animals.vista.Vista;
class CmdHabAdd extends Comanda {
    CmdHabAdd(String nom, String habitat) {
        addArgument("nom", nom);
        addArgument("habitat", habitat);
    }
    CmdHabAdd(String nom, String categoria, String habitat) {
        this(nom,habitat);
        addArgument("categoria", categoria);
    }
    public String getNomComanda() { return "add"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String categoria = getValorDArgument("categoria");
        String habitat = getValorDArgument("habitat");
        int nouHabitat = 0;
        String resultat = "";
        try{
            nouHabitat = model.addHabitat(nom, categoria, habitat);
        }catch(SQLException s){
            System.out.println("Error :" + s);
        }
        
        resultat = vista.composaResposta(nouHabitat);
        System.out.println(resultat);
        return false;

    }
}





