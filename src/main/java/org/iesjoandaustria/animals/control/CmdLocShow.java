package org.iesjoandaustria.animals.control;

import java.sql.*;
import org.iesjoandaustria.animals.model.Model;
import org.iesjoandaustria.animals.vista.Vista;
class CmdLocShow extends Comanda {
    public CmdLocShow(String nom){
        addArgument("nom", nom);
    }

    public CmdLocShow(String nom, String categoria){
        this(nom);
        addArgument("categoria", categoria);
    }

    public String getNomComanda() { return "CmdLocUnset"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String categoria = getValorDArgument("categoria");
        String resultat = "";
        try{
            String localitzacio = model.showLoc(nom, categoria);
            if(localitzacio == null){
                resultat = vista.composaResposta(-6);
            }else{
                if(localitzacio.equals("-3")){
                    resultat = vista.composaResposta(-3);
                }else{
                    resultat = vista.composaLocalitzacio(localitzacio);
                }
            }

            System.out.println(resultat);
        }catch(SQLException s){
            System.out.println("Error: " + s);
        }
        return false;
    }

}
