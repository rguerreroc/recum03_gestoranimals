package org.iesjoandaustria.animals.control;
/*
 * Comanda Del
 */
import java.sql.*;
import org.iesjoandaustria.animals.model.*;
import org.iesjoandaustria.animals.vista.Vista;
class CmdDel extends Comanda {
    public CmdDel(String nom) {
        addArgument("nom", nom);
    }
    public CmdDel(String nom, String categoria) {
        addArgument("nom", nom);
        addArgument("categoria", categoria);
    }
    public String getNomComanda() { return "del"; }
    public boolean executa(Model model, Vista vista) {
        String nom = getValorDArgument("nom");
        String cat = getValorDArgument("categoria");
        String resultat = "";
        try{
            int codOperacio = model.delSelectedAnimal(nom, cat);
            resultat = vista.composaResposta(codOperacio);

            System.out.println(resultat);



        }catch(SQLException s){
            System.out.println("Error: "+s);
        }

        return false;
    }
}





