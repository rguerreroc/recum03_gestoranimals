package org.iesjoandaustria.animals.bd;
/*
 * Implementació de la capa de base de dades
 */
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.iesjoandaustria.animals.model.Animal;
import java.util.*;
public class Bd {
    public Bd(){
        try{
            this.connecta();
        }catch(SQLException s){
            System.out.println("Error de conexió amb la BDD: " + s);
        }
    }
    private Connection conn = null; // connexió

    public void connecta() throws SQLException {
        if (conn == null) {
            String usuari = "usuaribd";
            String password = "pass";
            String host = "jdadaw.sytes.net";
            String bd = "testbd";
            String url = "jdbc:postgresql://" + host + "/" + bd;
            conn = DriverManager.getConnection(url,usuari,password);
            System.out.println("Connectat amb " + host);
        }
    }

    private void desconnecta() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
            }
            System.out.println("Desconnectat");
            conn = null;
        }
    }

    public List<Animal> selectTotsElsAnimals() throws SQLException{
        String sql = "SELECT * FROM animals ORDER BY categoria, nom";
        Statement st = null;
        List<Animal> llistaAnimals = new ArrayList<Animal>();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int contAnimals = 0;
            while (rs.next()) {
                int id = rs.getInt("id");
                String nom = rs.getString("nom");
                String cat = rs.getString("categoria");
                String repro = rs.getString("reproduccio");
                Animal animal = new Animal(id, nom, cat, repro);
                llistaAnimals.add(animal);
                contAnimals++;
            }
            if (contAnimals == 0){
                System.out.println("Cap animal de moment");
                rs.close();
            }
        }catch(SQLException s){
            System.out.println("Error: "+s);
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return llistaAnimals;
    }

    public List<Animal> selectAnimalsByNom(String nom) throws SQLException{
        String sql = "SELECT * FROM animals WHERE nom = '" + nom + "'";
        Statement st = null;
        List<Animal> llistaAnimals = new ArrayList<Animal>();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            int contAnimals = 0;
            while (rs.next()) {
                int id = rs.getInt("id");
                String cat = rs.getString("categoria");
                String repro = rs.getString("reproduccio");
                Animal animal = new Animal(id, nom, cat, repro);
                llistaAnimals.add(animal);
                contAnimals++;
            }
            if (contAnimals == 0){
                System.out.println("Cap animal de moment");
                rs.close();
            }
        }catch(SQLException s){
            System.out.println("Error: "+s);
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return llistaAnimals;
    }
    public boolean existAnimal(Animal a) throws SQLException {
        String sql = "SELECT count(*) FROM animals WHERE nom='" + a.getNom()
            + "' AND categoria='" + a.getCategoria() + "'";
        Statement st = null;
        int id;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            id = rs.getInt(1);
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }

        if (id == 0) {
            return false;
        } else {
            return true;
        }
    }

    public int existsAnimal(String nom) throws SQLException {
        String sql = "SELECT count(*) FROM animals WHERE nom='" + nom + "'";
        Statement st = null;
        int id;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            id = rs.getInt(1);
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }

        return id;
    }

    public int addAnimal(Animal a) throws SQLException{
        boolean exist = existAnimal(a);
        if(!exist){
            String sql = "INSERT INTO animals (nom, categoria, reproduccio) VALUES("
                + "\'"
                + a.getNom()
                + "\', "
                + "\'"
                + a.getCategoria() 
                + "\', " 
                + "\'" 
                + a.getReproduccio() 
                + "\')";
            Statement st = null;

            try {
                st = conn.createStatement();
                int num = st.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

                // afegits = "Nombre d'animals afegits " +num+"\n";
                ResultSet rs = st.getGeneratedKeys();
                rs.next();
                int id = rs.getInt(1);
                a.setId(id);
                rs.close();

            } finally {
                if (st != null) {
                    st.close();
                }
            }
            return 0;
        }else{
            return -1;
        }
    }

    public int preDelAnimal(String nom, String categoria) throws SQLException{
        int exists = existsAnimal(nom); // 0 no encontrado | 1+ encontrado
        if(categoria == null){
            if(exists > 1){
                return -3;
            }else if(exists == 1){
                delAnimal(nom);
                return 0;
            }else{
                return -2;
            }
        }else{
            delAnimal(nom, categoria);
            return 0;
        }
    }


    public void delAnimal(String nom) throws SQLException{
        String sql = "DELETE FROM animals WHERE nom = '" + nom + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }

    }

    public void delAnimal(String nom, String categoria) throws SQLException{
        String sql = "DELETE FROM animals WHERE nom = '" + nom + "' AND categoria = '" + categoria + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }


    public int preAssingCat(String nom, String catDest, String catOri) throws SQLException{
        int exists = existsAnimal(nom); // 0 no encontrado | 1+ encontrado
        List<Animal> animalCat = selectAnimalsByNom(nom);
        if(catOri == null){
            if(exists > 1){
                return -3;
            }
            for(Animal a: animalCat){
                if(catDest.equals(a.getCategoria())){
                    return -4;
                }
            }
            assingCat(nom, catDest);
            return 0;
        }else{
            int catFound = -5;
            for(Animal a: animalCat){
                if(catDest.equals(a.getCategoria())){
                    return -4;
                }
                if(catOri.matches(a.getCategoria())){
                    catFound = 0;
                }
            }
            if(catFound == 0){
                assingCat(nom, catDest, catOri);
                return 0;
            }else{
                return catFound;
            }
        }
    }

    public void assingCat(String nom, String categoria) throws SQLException{
        String sql = "UPDATE animals SET categoria = '" + categoria + "' WHERE nom = '" + nom + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public void assingCat(String nom, String catDest, String catOri) throws SQLException{
        String sql = "UPDATE animals SET categoria = '" + catDest + "' WHERE nom = '" + nom + "' AND categoria = '" + catOri + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public int preSetLoc(String nom, String categoria, String sala, String passadis, String armari, String prestatge) throws SQLException{
        int exists = existsAnimal(nom);
        if(categoria == null){
            if(exists > 1){
                return -3;
            }
            setLoc(nom, sala, passadis, armari, prestatge);
            return 0;
        }else{
            setLoc(nom, categoria, sala, passadis, armari, prestatge);
            return 0;
        }
    }

    public void setLoc(String nom, String sala, String passadis, String armari, String prestatge) throws SQLException{
        String sql = "UPDATE animals SET localitzacio = ROW('"
            + sala +"'," + passadis +"," + armari +"," + prestatge +")::localitzacio "
            + "WHERE nom='" + nom + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public void setLoc(String nom, String categoria, String sala, String passadis, String armari, String prestatge) throws SQLException{
        String sql = "UPDATE animals SET localitzacio = ROW('"
            + sala +"'," + passadis +"," + armari +"," + prestatge +")::localitzacio "
            + "WHERE nom='" + nom 
            + "' AND categoria = '" + categoria + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public int preUnsetLoc(String nom, String categoria) throws SQLException{
        int exists = existsAnimal(nom);

        if(categoria == null){
            if(exists > 1){
                return -3;
            }else{
                unsetLoc(nom);
                return 0;
            }
        }else{
            unsetLoc(nom, categoria);
            return 0;
        }
    }

    public void unsetLoc(String nom) throws SQLException{
        String sql = "UPDATE animals SET localitzacio = null WHERE nom = '" + nom + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public void unsetLoc(String nom, String categoria) throws SQLException{
        String sql = "UPDATE animals SET localitzacio = NULL WHERE nom = '" + nom + "' AND categoria = '" + categoria + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            int num = st.executeUpdate(sql);

        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    public String preShowLoc(String nom, String categoria) throws SQLException{
        int exists = existsAnimal(nom);
        String locAnimal;;
        if(categoria == null){
            if(exists > 1){
                return "-3";
            }else{
                locAnimal = showLoc(nom);
            }
        }else{
            locAnimal = showLoc(nom, categoria);
        }
        return locAnimal;
    }

    public List<String> preShowHab(String nom, String categoria) throws SQLException{
        int exists = existsAnimal(nom);
        List<String> habAnimal = new ArrayList<String>();
        if(categoria == null){
            if(exists > 1){
                habAnimal.add("-3");
            }else{
                habAnimal = showHab(nom);
            }
        }else{
            habAnimal = showHab(nom, categoria);
        }
        return habAnimal;
    }

    public String showLoc(String nom) throws SQLException{
        String sql = "SELECT localitzacio FROM animals WHERE nom = '" + nom + "'";

        Statement st = null;
        String loc;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            loc = rs.getString(1);
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return loc;
    }

    public List<String> showHab(String nom) throws SQLException{
        String sql = "SELECT habitat FROM animals WHERE nom = '" + nom + "'";

        Statement st = null;
        List<String> hab = new ArrayList<String>();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                String nomHab = rs.getString(1);
                hab.add(nomHab);
            }
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return hab;
    }

    public String showLoc(String nom, String categoria) throws SQLException{
        String sql = "SELECT localitzacio FROM animals WHERE nom = '" + nom + "' AND categoria = '" + categoria + "'";

        Statement st = null;
        String loc;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            loc = rs.getString(1);
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return loc;
    }

    public List<String> showHab(String nom, String categoria) throws SQLException{
        String sql = "SELECT habitat FROM animals WHERE nom = '" + nom + "' AND categoria = '" + categoria + "'";

        Statement st = null;
        List<String> hab = new ArrayList<String>();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                String nomHab = rs.getString(1);
                hab.add(nomHab);
            }
                rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return hab;
    }

    public Animal preRepoAnimal(String nom, String categoria) throws SQLException{
        int exists = existsAnimal(nom);
        Animal a = null;
        if(categoria == null){
            if(exists == 1){
                a = repoAnimal(nom);
            }else if(exists == 0){
                a = new Animal(-2, null, null, null);
            }else{
                a = new Animal(-3, null, null, null);
            }
        }else{
            a = repoAnimal(nom, categoria);
        }
        return a;
    }

    public Animal repoAnimal(String nom) throws SQLException{
        Animal a = null;
        String sql = "SELECT * FROM animals WHERE nom = '" + nom + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
                int id = rs.getInt("id");
                String nomA = rs.getString("nom");
                String cat = rs.getString("categoria");
                String repro = rs.getString("reproduccio");
                a = new Animal(id, nomA, cat, repro);

        }catch(SQLException s){
            System.out.println("Error: "+s);
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return a;
    }

    public Animal repoAnimal(String nom, String categoria) throws SQLException{
        Animal a = null;
        String sql = "SELECT * FROM animals WHERE nom = '" + nom + "'AND categoria = '" + categoria + "'";

        Statement st = null;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            int id = rs.getInt("id");
            String nomA = rs.getString("nom");
            String cat = rs.getString("categoria");
            String repro = rs.getString("reproduccio");
            a = new Animal(id, nomA, cat, repro);

        }catch(SQLException s){
            System.out.println("Error: "+s);
        } finally {
            if (st != null) {
                st.close();
            }
        }
        return a;
    }

    public int preAddHabitat(String nom, String categoria, String habitat) throws SQLException{
        int exists = existsAnimal(nom);
        int codOp = 0;
        if(categoria == null){
            if(exists != 1){
                if(exists > 1){
                    return -3;
                }else{
                    return -2;
                }
            }else{
                codOp = addHabitat(nom, habitat);
            }
        }else{
            codOp = addHabitat(nom, categoria, habitat);
        }
        return codOp;
    }

    public boolean chkHab(String nom, String habitat) throws SQLException{
        String sql = "SELECT count(*) FROM animals WHERE nom = '" + nom + "'AND habitat @> '" + habitat + "'";
        Statement st = null;

        int id;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            id = rs.getInt(1);
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }

        if (id == 0) {
            return false;
        } else {
            return true;
        }

    }

    public boolean chkHab(String nom, String categoria, String habitat) throws SQLException{
        String sql = "SELECT count(*) FROM animals WHERE nom = '" + nom + "' AND categoria = '" + categoria + "' AND habitat @> '" + habitat + "'";
        Statement st = null;

        int id;
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs.next();
            id = rs.getInt(1);
            rs.close();
        } finally {
            if (st != null) {
                st.close();
            }
        }

        if (id == 0) {
            return false;
        } else {
            return true;
        }

    }

    public int addHabitat(String nom, String habitat) throws SQLException{
        boolean chk = chkHab(nom,habitat);
        Statement st = null;

        if(chk){
            return -7;
        }else{
            //String sql = "UPDATE animals SET habitat = append_array(habitat, ARRAY['" + habitat + "']) WHERE nom = '" + nom + "'";
            //String sql = "UPDATE animals SET habitat = array_append(habitat, ROW('" + habitat + "')::text) WHERE nom = '" + nom + "'";
            String sql = "UPDATE animals SET habitat = habitat || ROW('"+ habitat +"')::text WHERE nom = '"+nom+"'";

            System.out.println("XXX " + sql);
            int id;
            try {
                st = conn.createStatement();
                int num = st
                    .executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = st.getGeneratedKeys();
                rs.next();
                id = rs.getInt(1);
                rs.close();
            } finally {
                if (st != null) {
                    st.close();
                }
            }
            return 0;
        }

    }

    public int addHabitat(String nom, String categoria, String habitat) throws SQLException{
        boolean chk = chkHab(nom,habitat);
        Statement st = null;

        if(chk){
            return -7;
        }else{
            String sql = "UPDATE animals SET habitat = array_append(habitat, ROW('" + habitat + "')::text) WHERE nom = '" + nom + "' AND categoria = '" + categoria + "'";

            System.out.println("XXX " + sql);
            int id;
            try {
                st = conn.createStatement();
                ResultSet rs = st.executeQuery(sql);
                rs.next();
                id = rs.getInt(1);
                rs.close();
            } finally {
                if (st != null) {
                    st.close();
                }
            }
            return 0;

        }
    }
}
