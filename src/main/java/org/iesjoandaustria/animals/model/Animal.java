package org.iesjoandaustria.animals.model;
/*
 * Implementa la classe Animal
 * Compte: Potser no 
 */
public class Animal {
    private int id;     // valor de la clau primària a ANIMALS
    private String nom;
    private String categoria;
    private String reproduccio;
    public Animal(String nom, String categoria, String reproduccio) {
        this(-1, nom, categoria, reproduccio); // encara no té identificador
    }
    public Animal(int id, String nom, String categoria, String reproduccio) {
        this.id = id;
        this.nom = nom;
        this.categoria = categoria;
        this.reproduccio = reproduccio;
    }
    public void setId(int id)    { this.id=id;       }
    public String getNom()       { return nom;       }
    public int getId()       { return id;       }
    public String getCategoria() { return categoria; }
    public String getReproduccio() { return reproduccio; }
    public String toString(){
        return "(" + id + ", '" + nom + "', '" + categoria + "', '" + reproduccio +"')";
    }
}
