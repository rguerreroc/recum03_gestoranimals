package org.iesjoandaustria.animals.model;
/*
 * Implementació de la capa de model
 */
import org.iesjoandaustria.animals.bd.Bd;
import java.util.*;
import java.sql.*;

public class Model {
    private Bd bd = new Bd();

    public List<Animal> getLlistaTotsElsAnimals() throws SQLException{
        return bd.selectTotsElsAnimals();
    }

    public int addNouAnimal(Animal a) throws SQLException{
        return bd.addAnimal(a);
    }

    public int delSelectedAnimal(String nom, String categoria) throws SQLException{
        return bd.preDelAnimal(nom, categoria);
    }

    public int assingCat(String nom, String catDest, String catOri) throws SQLException{
        return bd.preAssingCat(nom, catDest, catOri);
    }

    public int addHabitat(String nom, String categoria, String habitat) throws SQLException{
        return bd.preAddHabitat(nom, categoria, habitat);
    }

    public int setLoc(String nom, String categoria, String sala, String passadis, String armari, String prestatge) throws SQLException{
        return bd.preSetLoc(nom, categoria, sala, passadis, armari, prestatge);
    }

    public int unsetLoc(String nom, String categoria) throws SQLException{
        return bd.preUnsetLoc(nom, categoria);
    }

    public String showLoc(String nom, String categoria) throws SQLException{
        return bd.preShowLoc(nom, categoria);
    }

    public List<String> showHab(String nom, String categoria) throws SQLException{
        return bd.preShowHab(nom, categoria);
    }

    public Animal repoAnimal(String nom, String categoria) throws SQLException{
        return bd.preRepoAnimal(nom, categoria);
    }

    public String searchLoc(String nom, String categoria) throws SQLException{
        return bd.preShowLoc(nom, categoria);
    }
}

