package org.iesjoandaustria.animals.vista;
/*
 * Implementació de la capa de vista
 */
import java.util.*;
public class Vista {

    public String composaLlistatAnimals(Map<String, List<String[]>> animals) {
        String resposta = "";
        for (String category : animals.keySet()) {
            // String resposta = ("*Primera categoría per ordre alfabètic");
            resposta += category + ":\n";
            for (String[] parell : animals.get(category)) {
                String nom = parell[0];
                String repo = parell[1];
                resposta += "\t" + nom + " (" + repo + ")\n";
            }
        }
        return resposta;
    }

    public String composaComiat(){
        return "Adeeu!";
    }

    public String composaLocalitzacio(String loc){
        String[] splitLoc = loc.split(",");
        String resposta = "Localitzacio:\n\tSala => " + splitLoc[0] 
            + "\n\tPassadis => " + splitLoc[1]
            + "\n\tArmari => " + splitLoc[2]
            + "\n\tPrestatge => " + splitLoc[3];

        return resposta;
    }

    public String composaHabitat(List<String> hab){
        
        String resposta = "Habitat:\n\t";
            for(String habitat : hab){
                resposta += habitat + "\n\t";
            }
        return resposta;
    }

    public String composaAnCatRepo(List<String> a){
        String resposta = "";



        resposta = "Animal:\n\t" + a.get(0)
                + "\nCategoria:\n\t" + a.get(1)
                + "\nReproducció:\n\t" + a.get(2);

        return resposta;
    }

    public String composaResposta(int i){
        String resMsg = "";
        //System.out.println("vista cod Op: " + i);
        switch (i){
            case -8:
                resMsg = "L'animal no te habitat asignat";
                break;
            case -7:
                resMsg = "L'animal ja te aquest habitat";
                break;
            case -6:
                resMsg = "L'animal no te localitzacio asignada";
                break;
            case -5:
                resMsg = "L'animal no te aquesta categoría d'origen";
                break;
            case -4:
                resMsg = "L'animal ja te aquesta categoría";
                break;
            case -3:
                resMsg = "L'animal te massa categories indica una";
                break;
            case -2:
                resMsg = "Animal inexistent";
                break;
            case -1:
                resMsg = "Animal ja existent";
                break;
            case 0:
                resMsg = "Operació realitzada amb èxit";
                break;

        }
        return resMsg;

    }
}
